---
layout: markdown_page
title: "Accounting"
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

# Accounting

This page contains GitLab's accounting policies, procedures and guidelines.

## Accounting and Finance Procedures

The below documentation contain step-by-step instructions on how to execute various Accounting team responsibilities. Our goal is to complete the monthly accounting close process within 10 business days from the last day of the month. The link to our month-end close checklist can also be found below.

## Accounts Payable, Employee Expenses, and Other Liabilities

### NetSuite

Invoices will arrive by email to ap@gitlab.com.

1. Forward email to the appropriate GitLab team member for approval.
1. Create a PDF copy of the email containing the approval response.
1. File the invoice and approval in the "Invoices" folder in Google Drive.
1. Enter the invoice in NetSuite. Step by step instructions for this process are below.

#### Entering a Bill (invoice) in NetSuite

1. On the home page, click the “+” icon near the global search bar at the top of the screen and select “Bill."
1. Select the appropriate vendor record. If adding a new vendor, follow the bullets below before proceeding, otherwise skip to step 3.
    * Enter the company name, email address, applicable subsidiary, physical address, payment terms, primary currency, and Tax ID. (Note that the address field is located under the "Address" tab, while the Tax ID, primary currency, and payment terms fields are located under the "Financial" tab)
    * Enter the banking information in the "Comments" field then click “Save.”
    * Go to the "+" icon at the top of the vendor record and select "Bill" from the dropdown box.
1. Enter Bill date. The due date should auto-fill based on payment terms entered during vendor setup. If not, select the correct due date and update the vendor record after the bill has been entered and saved.
1. Enter Bill number.
1. Go to the "Expense and Items" tab below to enter the expense details.
1. Select appropriate GL-account under the "Account" dropdown box. (Be sure to check whether the invoice represents a prepaid expense, fixed asset, etc.)
1. Enter Bill amount.
1. Select tax code, if applicable.
1. Enter department. (This must be entered if the account you selected in step 6 is an expense account)
1. Add attachments: Go to the "Communication" tab and find the "Files" subtab.
1. Click "New File.” A new window will appear, allowing you to select the file you wish to attach.
1. In the new window, select the "Attachments Received" folder in the dropdown box, then click "Choose File" to attach both a copy of the vendor bill and email approval. (The supporting email approval must be attached along with a copy of the invoice)
1. Click "Save.”
1. In Google Drive, file invoice in the “Unpaid” folder.

### Processing payment for invoices

1. Upon receipt of vendor invoices:
    * File a .pdf copy of the invoice to dropbox\For Approval.
    * Notify manager of new invoices to be approved by forwarding the email from the vendor.
    * Invoices are to be approved based on signed agreements, contract terms, and or purchase orders.
    * After review, manager to reply to email with “Approved”. An audit trail is required and this email will serve this purpose.
1. On approval, move the invoice from dropbox\For Approval to dropbox\Inbox
1. Post the invoice through accounting system. Before paying any vendor (for Inc. only), be sure there is a W-9 on file for them.
1. On a daily basis, generate an AP aging summary from the accounting system and identify invoices to be paid.
1. Initiate payment(s) through the bank (Comerica/Rabobank) and notify management that there is a pending payment.  Include a summary of invoices being paid.
1. Verify the payment has cleared the bank.
1. Upon verified payment of the invoice move the .pdf copy of the invoice from dropbox\Inbox to folder inbox\”vendor name”.
1. Post the payment through the accounting system.

### Processing Expensify Reports 

1. In Expensify, go to the “Reports” tab.
1. Filter the listed reports and narrow the results to reports that are marked as “Reimbursed.”
1. Click the checkbox next to each report that you wish to export to NetSuite.
1. Once the reports have been selected, go to the “Export to” dropdown menu near the top of the page and click “NetSuite.”
1. A box will appear showing the real-time status of the export. The process is complete once this box confirms a successful export. If you receive an error message, see the information below and follow the remaining steps.
    * Unsuccessful exports typically result from an error related to NetSuite records. Each Expensify user must have a corresponding NetSuite Employee record and Vendor record.
    * Check NetSuite to verify if the employee in question has both a Vendor and Employee record.
    * If the records exist, add the employee’s email address to both records. In order for the export to work, this must be the same email address that is assigned to the employee within Expensify.
    * If the records do not exist, create both an Employee and Vendor record in NetSuite, complete with email addresses. Expensify uses the employee email address when mapping its records to that of NetSuite. The export will not work without proper alignment of email addresses in the NetSuite and Expensify records.

### Recording Employee Stock Option Expense

Employee stock options are recorded on a calendar quarterly basis. The puropse of this entry is to record the expense and allocate it between the appropriate departments.

1. Request a copy of the Carta report named "GitLab-Inc 20XX Equity Incentive Plan Grant Date Annual US GAAP Consolidated" from the CFO (the report should be in CSV/Excel format).
1. We will focus on the Expense Summary and Date tabs
1. On the Expense Summary tab, click on the journal entry amount to see column and tabs being referenced in the formula. 
    * This should be column AX of the Date tab, and this column will be used to allocate the total expense between departments.
1. Sort the columns on the Date tab and delete all rows with a zero balance 
1. Add a column to the Date tab with department information that corresponds to the employees referenced in column B (department information can be found in NetSuite).
1. Create a pivot table using the data from column AX and the department column created in the previous step.
    * The pivot table should provide the amount of stock option expense attributable to each department.
1. Check that the total amount of the pivot table matches the amount on the Expense Summary tab.
1. Proceed to NetSuite to create a journal entry that matches the Expense Summary tab.
1. Date the journal entry using the last day of the quarter being closed. 
1. The debit side of the entry uses GL "6077 Stock Compensation Expense" for all departments other than Cost of Sales which uses GL "5090 Stock Compensation Cost of Sales."
1. Be sure to fill in the Department attribute when adding lines to the journal entry.
1. The credit side of the entry only needs one line and uses GL "3015 Additional Paid in Capital- Stock Options"
1. Save the report used to calculate the journal entry and attach it to the entry within NetSuite.
1. Click Save and the entry is complete.

## Accounts Receivable
<a name="ar"></a>

### Invoicing

1. Log in to Zuora and Salesforce.
1. In Salesforce, click the "+" icon and select "Z-Quotes".
1. Subscriptions to be invoiced are sorted by date. Choose a subscription from today's date and proceed.
1. Still in Salesforce, check the customer account for a current PO number under the notes and attachments.
1. Then in Zuora, search for the corresponding customer subscription and open the record.
1. Check to see if the PO from the file in Salesforce matches that of Zuora. If not, update the record in Zuora with the latest PO number.
1. Verify that the "Bill to" and "Sold to" contacts listed in the Zuora customer record match the related record in Salesforce. If not, update Zuora.
1. Verify that the payment terms and method listed in Zuora match the related record in Salesforce. If not, update Zuora.
1. In Zuora, scroll down to transactions and click "Create Bill Run."
1. It is important to find the subscription start date in Salesforce and enter it in the "Target Date for this bill run" field in Zuora. The invoice date should be today's date.
1. Once these dates agree, click "Create Bill Run."
1. On the following screen, select the bill run that was just created (the bill run files begin with BR- followed by a set of numbers).
1. Scroll down and click on the invoice that was just generated. The PDF file will be at the bottom of the page.
1. Open the PDF file and review every field of the invoice for accuracy.
1. Once the review is complete, close the invoice and click “Post Invoice.”
1. Proceed to bill the remaining subscriptions.

At this point the invoicing process is complete. Now, continue on to the Cash Receipt posting process for those invoices that were paid by credit card.

### Amendments to Subscriptions
<a name="Amendments"></a>

To amend a customer's account, choose one of the options below from the subscription page in Zuora.

1. Terms and Conditions - used to change the end date of a product
2. New Product - choose when adding a product
3. Update a product - choose when making a change to a current product
4. Remove a product - used when removing a product
5. Renewal 

### Cash Receipt
<a name="cash-receipt"></a>

### Credit card customer

Follow this procedure if the customer paid by credit card.
You may recall from the invoicing process that there was still a balance due when saving the invoice.  The following steps will record the payment and remove the balance due.

1. Login to Stripe dashboard and click on Payments under Transactions (left hand side). You will see a listing of the latest Stripe transactions listed by amount, Recurly transaction, name, date, and time. There is also an option to filter the report by clicking on XXX at the top left. Click on XXX to export to excel. This will give you a workbook area and also a breakdown of the fees which we will work on later.
1. In NetSuite, click on the "Transactions" tab on the left.
    * Click on the orange "OPEN INVOICES " tab. This will bring up all open invoices listed by date, invoice #, customer, etc.
1. Match invoice #s  between the Stripe dashboard and NetSuite. If you click on a transaction in the Stripe dashboard, it will take you to a screen that shows more detail, including the invoice # being paid. You can work your way from the bottom up.
1. In NetSuite, click "Receive Payment" on the matched payment and invoice.
1.  Receiving the payment
    * Enter the payment date, which is the payment date from Stripe dashboard.
    * Payment method = Credit Card.
    * Reference no. = "Recurly Transaction ID:" found under Metadata in Stripe dashboard.
    * Deposit to = Stripe.
    * NetSuite will auto-fill the payment amount with the entire balance due. No need to change this unless the payment amount from Stripe is different.
    * Click on "Save and Close".
    * Repeat the above for all the remaining invoices that were paid by credit card.

1. Post a journal entry to record Stripe Fees.
    * In NetSuite, click on the "+" sign. Under "Other", select "Journal Entry".
    * It is okay to leave the journal date as long as it is within the month the fees were incurred. If not, change it to the last day of the month.
    * NetSuite will auto fill the journal number. Do not change.
    * Account #1 Entry
      * Fill the "Account #1" entry with "Credit Card Transaction fees".
      * Fill the "Debits" entry with the value from the Stripe report that was exported. The value will be the sum of "Column I" in the Stripe report, which is the fee amount. Be sure to only sum the rows which you just posted payments for.
      * Leave the "Credits" entry empty.
      * Fill the "Description" entry with "To record credit card transaction fees for period (enter the date range for the transactions posted)".
      * Leave the "Name" entry empty.
      * Fill the "Class" entry with "Sales".
    * Account #2 Entry
      * Fill the "Account #2" entry with "Stripe".
      * Leave the "Debits" entry empty.
      * The "Credits" entry will autofill. This should be the same amount as the "Debits" entry for Account #1.
      * The "Description" entry will autofill. This should be the same as the "Description" entry for Account #1.
      * Leave the "Name" entry blank.
      * Leave the "Class" entry blank.
      * Click "Save".

This transaction transfers the payment obligation from the customer to Stripe.  The payment obligation from Stripe is removed when Stripe transfers  the funds to GitLab's bank account.

### Posting a payment from Stripe when a transfer is received from Stripe.

Post a journal entry:
1. Fill the "Journal Date" with the date that payment was received in the bank.
1. Fill the "Credit Account" with Stripe.
1. Fill the "Debit Account" with "Comerica Checking - GitLab Inc."
1. Leave "Name" blank.
1. Leave "Class" blank.
1. Fill the "Description" with "To record Stripe transfer (date of transfer)".
1. Click "Save".


### Posting a payment from a “bank customer”

In Netsuite:
1. Click on the “+” sign.
1. Click on “Receive Payment” under Customers.
1. Fill the "Payment Date" with the date payment was received.
1. Fill the "Payment Method" choose from the dropdown menu.
1. Fill the "Reference No." with the check # or bank reference # from incoming wire.
1. Fill the "Deposit to" with "Comerica Checking".
1. Fill the "Amount Received" with the amount received from the incoming wire.

### Refunds

### How to initiate a refund request

1. Complete and submit the following form: 
   * https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000258393
   * A GitLab employee can submit a refund request on behalf of the customer or you can direct the customer to complete the form.
   * A support ticket will be created and will be routed to AR@. 
   
### How to process a refund in Zuora

1. A refund request has been received by e-mail or refund opportunity in SFDC.
1. Log in to Zuora.
1. Search for the invoice that needs to be refunded.
1. On the invoice screen, scroll down to "Transaction(s) associated to this Invoice" and click on the payment associated to the invoice to be refunded.
1. On the payment screen, click on more and click on "Refund this payment."
1. Create the refund.
1. An auto-generated e-mail will be sent to the customer that the refund has been processed.
1. The payment will be reversed on the invoice and there will be a balance due.
1. Cancel the subscription in Zuora using the first day of the service period to be refunded.
1. Create a bill run for the subscription cancellation.
1. Post the invoice and transfer the credit balance.
1. Apply the credit balance to the original invoice.
1. Request a refund opportunity to be created in Salesforce if it hasn't been created already.
1. Enter in the refund invoice number and the date of the refund on the refund opportunity.

### How to process a partial refund in Stripe

1. Log in to Stripe.
1. Type in the cardholder’s name in the search field at the top of the screen.
1. Click on the original charge that will be refunded.
1. Click on the refund button.
1. Enter in the amount to refund.
1. Enter in the reason code.
1. In the description field, enter the reason for the refund and include the invoice that was created in Zuora for the refunded amount.
1. Click on refund.
1. Go back to Zuora.
1. Put the customer account in silent mode under "Communication Profile."
1. Post the invoice draft.
1. Transfer to credit balance.
   * Accounting Code: Stripe - Inc
1. Put the customer account back into Default/B2B under "Communication Profile."
1. Verify that the balance due is $0.00 on the refund invoice that was created.
1. Manually e-mail the customer, do not use Zuora to create the email as you cannot update the contents. Include a screenshot of the partial refund from Stripe and attach a copy of the refund invoice.

### Cash Collections
1. Zuora generates automatic payment reminders at 30, 60 and 90 days after invoice issuance
1. When an invoice reaches 30 days old an escalation email is sent to the account owner in sales.
1. When an invoice reaches 60 days old an escalation email is sent to the account owner in sales, the regional director and the CFO.
1. At 90 days old the account is put on support hold.  The billing specialist sends an email to support@gitlab.com with the customer information.
1. At 120 days old the account is put on credit hold and orders for that account can no longer be processed.
1. At 150 days old the account is sent to collections for recovery.
1. Self-managed accounts that were cancelled before the end of term will be put on credit hold and reviewed before another purchase.

### Cash Forecasting and Tracking

1. The Controller maintains the actual and forecast reporting for cash by entity.
2. The google sheet is updating weekly and reconciled to Netsuite once per month.
3. To find the internal google sheet search for "Cash Flow Forecast". If you need permission to access email the controller.

## Asset Tracking

Items paid for by the company are property of the company.

Assets with purchasing value in excess of [$1000 USD](/handbook/people-operations/global-compensation/#exchange-rates) are tracked in BambooHR, for assets of lower value we rely on the honor system. These assets come to People Ops attention by one of two ways: 1. People Ops makes the purchase on behalf of the team member, or 2. the Finance team notices the line items on expense reports / invoices and passes this along to People Ops.

The information is then entered into BambooHR (to track who has which piece of equipment) by People Ops, and it is included in the Fixed Asset Schedule by Finance (to track asset value and depreciation).

1. Go to the team member in BambooHR.
1. Click on the Assets Tab.
1. Click Update Assets.
1. Enter Asset Category, Asset Description, Serial Number, Asset Cost, and Date Loaned.
1. This process is repeated for each asset purchased.

If a team member would like to purchase an asset from the company (i.e. a laptop), please email People Operations to obtain the amount to be paid. This is derived from original cost less accumulated depreciation.

## Processing Payroll
<a name="payroll"></a>

### GitLab Inc. Payroll Procedures
<a name="payroll-inc"></a>

1. PeoplesOps will notify Payroll when new hires are added in BambooHR and I9 verification is completed
1. Payroll admin adds new team member into ADP via Payroll system only template
1. Email new hires with the ADP registration guide and ask them to update tax withholding and add direct deposit
1. All additional payment requests must received by the payroll changes due date.  The payroll schedule in ADP under Quick Links.
1. Sr. PeopleOps Analyst updates salary, department, job title, and managers in ADP WFN before or by the due date
1. Log into Betterment to run the current deferral rates report on the payroll processing date (5 days before check date)
1. Update new deferral rates to team member's record in ADP WFN under deduction
1. Lumity will send a benefits election change report by the 1st and the 15th of each month to payroll
1. Update/Add these benefits elections in ADP (be sure to enter employee and employer benefits premium)
1. Log into ADP WFN via Admin access
1. Start a new payroll cycle under Process and Payroll
1. Review the week # and check date
1. All salary team members are set up with autopaid for 86.67 hours per check date.
1. Create a batch for hourly employees, LOA, new hires, and/or termination
1. Create a batch for one time payment (referal bonus, discretion bonus, commission, SDR bonus, and quarterly bonus) as check number #2 or 3 with Bonus frequency
1. Create a batch for benefits corrections as needed
1. Be sure to enter W under Special action column in the one time payment batch to cancel the calculation of GTL
1. Generate payroll reports (employee changes, paydata summary) PDF format
2. Send payroll to preview
1. Review the preview register, make corrections as needed, and resend to preview
1. Accept payroll

All hourly time sheets are kept on the Google Drive and shared with Finance. Each employee will populate the time sheet before the end of the pay cycle.

#### One time payment

1. Create a batch and name it accordingly
1. Selecte the Bonus paydata grid
1. Add employee
1. Enter the earning type and amount
1. Enter B pay frequency
1. Enter #2, or 3 under pay #
1. Enter W under Special Action

#### Adding New Hire

1. PeopleOps will notify Payroll when I-9 verification is completed
1. Login to ADP as Administrator
1. Select Process, HR, and Hire/Rehire
1. Select Payroll Only (System) template
1. Enter the legal name from Passport or SSN in BambooHR
1. Select SSN for the Tax ID Type
1. Enter Hire Date
1. Select Gender
1. Reason for Hire – New Position
1. Enter Birth Date
1. Company Code – 26X
1. Select USA under the drop down under Countries
1. Enter address
1. Select Works from Home from the More Fields section on the right side
1. Select Yes for Works from Home and Use Primary Address as the Work Address
1. Select Ethnicity/Race ID Method under More Field
1. Look up the Ethnicity under Job section in BambooHR 
1. Enter Job Tile and Report to Manager
1. Select FT – Full Time under Worker Category
1. Select team member’s lived in state for Location
1. Select NAICS worker comp code – be sure to use 5302 for WA residents 
1. Enter work email address and check “Use For Notification”
1. Select Salary or hourly under Regular Pay Rate
1. Enter 86.67 hours for salary team members under Standard Hours and leave it blank for hourly members
1. Enter the Worked in State, Lived in State, and SUI/SDI tax code
1. Select Done
1. Email the ADP Registration email to the team member(s)

### Lumity Payroll Processes for GitLab Inc.

#### Payroll Process

Payroll files will be provided to GitLab by Lumity to the People Operations Analyst and the Payroll and Payments Lead. Lumity will send a “Diff” payroll file and ADP Upload file on each Payroll Send Date. GitLab will review the payroll schedule and notify Lumity if send dates need to be changed. The ADP Upload file will only contain standard per pay period deduction amounts. GitLab will add one time catch-up and/or credits through ADP using the DIFF file.

#### 401(k) Funding Process

1. Run the "401(k) contribution by check date" report in ADP WFN for current check date (must be 2 days before the check date)
1. Save a copy of the report in .xls format in this naming convention "401(k) contribution" check date
1. Total the traditional and Roth contributions and verify the amounts with the ADP register total report
1. Convert the file into .csv format
1. Log in to Betterment, https://app.bettermentforbusiness.com/plan_managers/sign_in
1. Select "Upload Payroll" on the landing page
1. Select "Upload Payroll", select the csv file, enter check date, and select "Upload File"
1. Betterment will verify the format of the uploaded file
1. Verify the contribution amounts against the ADP register total report
1. Approve the payroll contributions
1. Save a copy of the confirmation email of ACH request from Betterment
1. Upload the .cvs, .xls, ADP register total report, and email to Googledrive under GitLab Inc ->Payroll ->401k-Betterment -> year -> check date


#### Funding Process

Lumity will manage and fund all Discovery accounts. Employee and Employer funding will be taken from the payroll report provided to Lumity by GitLab.

* H.S.A
  * Employer contributions will be funded each payroll ($50 per pay)
  * Missed ER contribution will not have a catch up (Employee enrolls late…Lumity will only fund ER contribution on the upcoming pay period. Any missed employer contributions will be disregarded)
  * Discovery will debit GitLab Bank account on each funding date
  * Max out is allowed
* FSA
  * Funds will debit from the Discovery reserve account once the employee submits a claim
  * Discovery will contact GitLab if the reserve is low on funds
* Dependent Care FSA
  * Funds will debit from the Discovery reserve account once the employee submits a claim
  * Discovery will contact GitLab if the reserve is low on funds
* Limited FSA
  * Funds will debit from the Discovery reserve account once the employee submits a claim
  * Discovery will contact GitLab if the reserve is low on funds
* Commuter
  * Employee payroll deduction will occur on the last payroll of the month and funded on the 1st of the following month
  * Funds will debit from the Discovery reserve account once the employee submits a claim
  * Discovery will contact GitLab if the reserve is low on funds

### GitLab BV Pay Slip Distribution Process
<a name="payroll-bv"></a>

All GitLab BV employees receive their payslips within their personal portal of Savvy.
They can login and download their payslip to their computer if needed.

### UK, Belgium, Netherlands, Germany,& India Monthly Payroll Process

1. Payroll changes due date to the payroll providers is 15th - 17th.
1. PeopleOps will notify Payroll for all bonus payout requests, sick time, etc.
1. Payroll changes are entered into a spreadsheet for commission, bonus, new salary, expense (only for UK) and password protected the file
1. Payroll sends the payroll changes file to the local payroll providers.
1. Local payroll providers will send the payroll report to payroll@gitlab.com for review and approval
1. Payroll and Payments Lead will review and approve payroll before the 21st via email
1. Payroll save and upload the payroll report to the GoogleDrive by month and under the right entity
1. Payroll notify Financial Controller after approved payroll for Germany and Netherlands so he can queue up the ACH payments for net pay.
1. Once processed the payslips are sent electronically directly to the team members for them to access via a password protected system.

### SafeGuard Payroll Process

Payroll cut off for sending changes is at the beginning of each month. The process for submitting changes is as follows:


1. Payroll will make a copy of the [SafeGuard Payroll Changes](https://docs.google.com/spreadsheets/d/1VkRI3GuRpu4kMI9zQ_016TZiJMKWIZUSmRj_Bt6cYkM/edit#gid=1648114438) sheet and add the changes
1. Payroll will send a payroll changes file to SafeGuard on the 9th day of the month.  Due to confidentiality, the file will be password protected prior sending to SafeGuard.
1. The change sheet for that month should then be upload onto Google drive under the GitLab Inc => Payroll => PEO SafeGuard


### CXC Payroll Process

**For Australia**

The cutoff is typically the middle of the month and any salary changes will need to be sent by email along with a new Statement of Work (SOW). The process is as follows:

1. Payroll will make a copy of the [SOW template](https://docs.google.com/document/d/1Bs2fu-SHT8jiFcd2Y6QHvQeLdXaAxGbU47Nlqwum_v8/edit) and complete the details.
1. Once the SOW has been completed, verified and signed. Email the SOW to CXC
1. Payroll will email CXC for commission and bonus.

**For Canada**

Team members in Canada are paid fortnightly. Changes will need to sent by email to the CXC contact see Australia section for location of contact details. 

### Commission Payment Process
<a name="commission"></a>

1. Each sales person will receive their own calculation template.
1. Salesperson is to complete their monthly template four days (payroll will send reminder) prior to first payroll of the month. Upon completion, salesperson will ping a manager for review and approval.
1. Approving manager will ping accounting upon approval.
1. Accounting will review and reconcile paid vs unpaid invoices.
1. Accounting will note in calculation template the amounts to be paid in commission.
1. Accounting will ping payroll that commission calculation is complete.
1. Commission will get pay on the 1st check date of the month for US team members and at the end of the month for all other countries.

## Intercompany Calculation

1. Open Intercompany Calculation Sheet from Google Drive.
1. Add a new tab for the current month calculation. 
1. Copy the contents of the previous month’s tab to the current month and change the date in cell B2.
1. Access NetSuite and open the income statement (found under the Reports and Financial tabs).
1. Filter the income statement to the current month then further refine it by the desired subsidiary and set the column display to Subsidiary.
    * Note: The format of the income statement in NetSuite should look similar to that of the income statement in the Intercompany Calculation Sheet.
1. Starting with the GitLab Inc. consolidated entity, copy the income statement data from NetSuite to the corresponding income statement in the Intercompany Calculation Sheet.
    * The balance under GitLab.com department should be combined with the Marketing department when transferring data to the Intercompany Calculation Sheet.
    * The GitHost balance should be reported under the No Department column.
1. Repeat step 6 for the GitLab Inc (non-consolidated) and GitLab LTD subsidiaries. There is no need to perform this for GitLab BV as this balance will autofill. 
    * For GitLab LTD, you’ll need to convert the income statement from GBP to USD:
        * Start by the running the consolidated income statement in NetSuite and changing the column display filter to Subsidiary.
        * Then take net income from the GitLab LTD column and divide this balance by the net income found in the GitLab LTD income statement. The resulting GBP-USD exchange rate should be used to convert the income statement. 
1. Review the data for accuracy and verify that the formulas in the spreadsheet are up-to-date.
1. After reviewing, there should be no variances in column AL.
1. Update the balance in cell O100 so that the balance in cell R106 autocorrects to zero.
1. Update the exchange rate in cells O1110-O112 and cell O118 with this month’s rate*.
    * This exchange rate (USD to EUR) can be found in NetSuite under the Lists tab, then under the Accounting and Consolidated Exchange Rates subtabs.
1. Now, it’s time to create the intercompany vendor bills and customer invoices.
1. This can be tricky, so be sure to follow the workflow found in the Intercompany Calculation Sheet, starting under cell Q110 (the cells in column R reflect the exact names given in NetSuite, which is relevant for this exercise).
    * There will be two invoices and two bills issued between GitLab BV and GitLab Inc.
    * There will be one invoice and one bill issued between GitLab BV and GitLab LTD.
1. The amounts in cells N113 and N119 will be used to create the intercompany transactions between GitLab BV and GitLab Inc.
1. For increased speed and accuracy, start by finding an invoice (or bill) from the previous month, then use the Make Copy feature in NetSuite to create a duplicate.
    * This will allow you to review the data you are transferring, while comparing it to the previous month for additional guidance.
1. Complete the GitLab BV-GitLab Inc intercompany transactions by repeating step 15 for all of the required bills and invoices.
1. Next comes the GitLab BV-GitLab LTD transactions, which differ slightly from the GitLab BV-GitLab Inc transactions.
1. In NetSuite, download the GitLab LTD income statement in GBP.
1. Take the total overhead stated in GBP and add 6%. 
    * This will be reported in the invoice from GitLab BV to GitLab LTD.
1. Once again, repeat step 15 for the GitLab BV-GitLab LTD transactions.
1. When the transactions are complete, you can check your work by verifying that the affected Revenue and COGS accounts offset each other.

## Intercompany Settlement

Each calendar quarter, obligations between subsidiaries must be satisfied in accordance with their respective intercompany agreements.

1. Accounting Manager prepares the balances in the Intercompany Settlement Google sheet.
    * These balances derive from the AP and AR aging reports that are imported into the sheet.
1. Once all balances are finalized, the Accounting Manager sends to Controller for review and approval. Approvals are documented within the Google sheet.
1. Final approval by CFO.
1. Payments are then queued for disbursement from the relevant bank accounts.
1. Disbursements are conducted according to the [Signature authorization matrix](/handbook/finance/authorization-matrix).
1. Transaction is booked in NetSuite.

## Posting Swag Shop transactions in NetSuite
Transactions from the Swag Shop are remitted to the Comerica checking account daily and should be booked in NetSuite at the end of each month, per the guidelines below. Login credentials for Shopify and Avalara are located in 1Password.

1. In Shopify, download the transaction report in CSV format (found under Orders, then Export).
  * This report contains swag shop revenue and tax data to be recorded in NetSuite.
1. In Printfection, download the orders report (found under Reports, then Run a Report).
  * This report will be used to allocate transaction cost data between promotional, shipping, and merchandise.
1. In the Shopify transaction report, the amounts in column L reflect the total funds received from each order.
1. The total tax collected per transaction can be found in column K, with the supporting details found in columns BE-BH.
1. Create a journal entry in NetSuite under the GitLab Inc subsidiary, using the last day of the month as the entry date.
1. This journal entry has two parts- One to record the revenues and taxes, which are reported in Shopify, and the second part which includes shipping and merchandise cost information reported in Printfection.
  * Using the transaction report from Shopify, create line items for the revenue and various tax liabilities.
    * Carefully review your entries for tax liabilities to ensure they are accurately recorded under the appropriate GL accounts.
  * Using the orders report from Printfection, create line items for the costs of shipping and merchandise.
1. Click Save then proceed to final review of the entry.
1. The journal entry can be tested for accuracy by comparing the balances of the journal entry lines to the balances in the two reports.

### Importing Swag Shop data to Avalara

In [Shopify](https://www.shopify.com/login):
1. Click on "Orders" in the left-hand toolbar.
1. Select "Export" and the desired transaction history period.
1. Download the report in CSV format.
1. Copy the transaction data from the Shopify report to the [Avalara Tax Import template](https://drive.google.com/file/d/19_UBjfqKcnuOWDfHfRVfFRc5L35QF4Gf/view?usp=sharing) located in the Google Drive.
    * Note that the import will fail if the columns that are highlighted green are left blank. Also check to ensure that the state and local tax data is entered into the correct columns. This is required for tax compliance. 
1. Save file for the remaining steps below.

In [Avalara](https://admin-avatax.avalara.net/login.aspx?ReturnUrl=%2fAvaTax%2fTools%2fItemImportUpload.aspx):
1. Click on "Reports".
1. Select the "Import Data" option.
1. When prompted, choose the CSV import option.
1. Check the import status report to ensure all transactions are successfully imported.

## GitLab Summit Cost Tracking
Tracking expenses for company summits enables us to analyze our spend and find opportunities to iterate, and in turn, improve subsequent summits. To enable tracking we create an expense tag that will allow GitLabbers to tag summit related expenses in Expensify. This should be done prior to the announcement of each summit. 

In accordance with [Reimbursable Expense guidelines](/handbook/finance/accounting/#reimbursable-expenses), independent contractors should note which expenses are summit related on their invoices, prior to submitting to the company. 

#### In NetSuite:
1. Create customer profiles in NetSuite for each legal entity (currently, Inc, BV, LTD, GmbH). These will become the tags that are used in Expensify.
1. Go to the global search bar and click the *+* icon to add customer profiles. 
1. Create a profile for each of the four entities and use the following tag naming format: Location Summit 20XX Entity (e.g. Cape Town Summit 2018 Inc). **Be sure to do this for each legal entity.**
1. Log in to Expensify and follow the remaining steps. 

#### In Expensify: 
1. Go to the Admin page and click on the *Policies* tab.
1. There are six Expensify policies that we need to sync to NetSuite.
1. Select a policy and find the *Connections* subtab. The NetSuite connector is located on this page.
1. Click the *Sync Now* button for the NetSuite connector. The page will run a prompt showing sync status.     
1. Once the syncing process is complete, go to the *Tags* subtab.
1. Search for the tag by name (i.e. the NetSuite profiles created in prior steps) and ensure that the switch is in the "On" position. All other tags should remain off as to avoid confusion for those tagging expenses.
1. Create a test expense under each Expensify policy to verify that the summit tags work as planned. **Again, check to ensure that this process has been completed for each policy in Expensify.**

## Travel and Expense Guidelines

### Spend reduction

When reducing spend, we'll not take the easy route of (temporarily) reducing discretionary spending.
Discretionary spending includes expenses like travel, conferences, gifts, bonuses, merit pay increases and summits.
By reducing in these areas we put ourselves at risk of [increasing voluntary turnover among the people we need most](https://steveblank.com/2009/12/21/the-elves-leave-middle-earth-–-soda’s-are-no-longer-free/).
Discretionary spending is always subject to questioning, we are frugal and all spending needs to contribute to our goals.
But we should not make cuts in reaction to the need to reduce spend; that would create a mediocre company with mediocre team members.
Instead, we should do the hard work of identifying positions and costs that are not contributing to our goals.
Even if this causes a bit more disruption in the short term, it will help us ensure we stay a great place to work for the people who are here.

### Renting Cars in the United States and Canada

### Third Party Liability

Purchase the liability insurance that is excess of the standard inclusion of State minimum coverage in the rental agreement at the rental agency. GitLab’s insurance policy provides liability insurance for rental cars while conducting company business, but it may be excess over any underlying liability coverage through the driver or credit card company used to purchase the rental.

Purchase the liability offered at the rental counter if there are foreign employees renting autos in the US or Canada. While workers' compensation would protect an injured US employee, other passengers may have the right to sue. To ensure that GitLab has protection when a foreign employee invites another person into the car we recommend the purchase of this insurance when offered at the rental counter.

### Physical Damage – Collision Damage Waiver

**Do not** purchase the Collision Damage Waiver offered at the rental counter. GitLab purchases coverage for damage to rented vehicles.
 If travel to Mexico is required, **purchase** the liability insurance for Mexico offered at the rental counter. You should verify that the rental agreement clearly states that the vehicle may be driven into Mexico and liability coverage will apply.

## Renting Cars- Countries other than the US and Canada

### Third Party Liability

Purchase the liability insurance offered at the rental counter when traveling outside the US and Canada. Automobile Bodily Injury and Property Damage Liability insurance are required by law in almost every country. Please verify this coverage is included with the rental agreement.

### Physical Damage – Collision Damage Waiver

Purchase the Collision Damage Waiver or Physical Damage Coverage offered by the rental agency when traveling outside the US and Canada.

In the event of an accident resulting in damage to the rental car, the foreign rental agency will charge the credit card used to make the reservation with an estimated amount of repair costs if insurance is not purchased. If this happens, GitLab does not purchase Foreign Corporate Hired Auto Physical Damage Coverage to reimburse for damages.

### Personal Use by Employee or Family Members of Business Auto Rentals

Coverage is **not** provided for personal use of automobiles or when family members are driving. Please evaluate whether your own personal automobile insurance provides an extension for this coverage. If it does not, or you are renting a vehicle outside the US or Canada or taking a US rented vehicle into Mexico, we recommend that you purchase the liability and physical damage coverage offered by the rental agency to protect your personal liability when not engaged in company business. GitLab will not pay for liability or damage to the rental vehicle resulting from personal use or use by non-employees.


### Reimbursable Expenses

The procedure by which reimbursable expenses are processed varies and is dependent on contributor legal status (e.g. independent contractor, employee) and subsidiary assignment (Inc, LTD, BV, GmbH). Check with PeopleOps if you are unsure about either of these. 

For information regarding the company expense policy, check out the section of our team handbook on [Spending Company Money](/handbook/spending-company-money). The accounting team will review the expenses for compliance with the company travel policy.  Exceptions will be escalated to the manager for review and approval. The CEO will review selected escalations at least annually.

### Independent Contractors
Contributors who are legally classified as independent contractors should include reimbursable expenses on the invoices they submit to the company through our Accounts Payable mailbox (ap@gitlab.com). These team members should also consider the terms and conditions of their respective contractor agreements, when submitting invoices to the company.  

### Employees
Contributors who are on GitLab’s payroll are legally classified as employees. These team members will submit expenses in the Expensify application. Expense reports are to be submitted once a month, at least. If you were not assigned a user license as part of the onboarding process, you can request one by contacting PeopleOps. Additional information on getting started with Expensify and creating/submitting expense reports can be found [here.](https://docs.expensify.com/using-expensify-day-to-day/using-expensify-as-an-expense-submitter/report-actions-create-submit-and-close)

### Non-Reimbursable Expenses

In order to purchase goods and services on behalf of the company, you should first [consult the Signature Authorization Matrix](/handbook/finance/authorization-matrix/) to determine the approval requirements. Note that this **does not** include travel expenses and other incidentals. These expenses should be self-funded then submitted for reimbursement within Expensify, or in the case of independent contractors, included in invoices to the company (per the guidelines above). 

If approval is not required, then proceed and send the invoice to Accounts Payable (ap@gitlab.com). If approval is required, create a confidential issue in the [Finance Issue Tracker](https://gitlab.com/gitlab-com/finance) and tag the required functional and financial approvers. Most importantly, the team member making the purchase request is ultimately responsible for final review and approval of the invoices. Final review and approval are critical process controls that help ensure we don't make erroneous payments to vendors. All original invoices and payment receipts must be sent to Accounts Payable.

Examples of things we have not reimbursed:
1. Costume for end of summit party.
1. Boarding expense for dog while traveling.
1. Headphones costing $800 which were found to be in excess of our standard equipment guidelines.
1. Batteries for smoke detector.
1. Meals during the summit when team members opt out of the company provided meal option.
1. Cellphones and accessories. 

### Marketing Campaign Expenses
Please see the [campaign expense guidelines in the Marketing handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-operations/#campaign-cost-tracking).

### Company Credit Cards
<a name="company-cc"></a>

We have worked to reduce the number of outstanding company credit cards in an effort to centralize corporate purchasing, however, there are still certain situations where it may be more practical to issue additional corporate cards. This will be addressed on a case-by-case basis and final approval will come from the CFO. Please contact the Finance team if you would like further information on this. 

GitLabbers carrying company cards in their name are required to submit expenses in [Expensify](https://www.expensify.com/) on a monthly basis. Reports are to be submitted no later than the fourth business day of each month as the American Express statement period-end date is the 28th of each month. 

Card transactions are auto-imported into Expensify which then auto-generates the expense reports to match that of the American Express statements. Below you will find directions on how to sign up for an American Express account and submit these expenses in accordance with company policy.

### Creating an American Express account

Once you receive your card, [register the card in the American Express portal.](http://www.americanexpress.com/confirmcard)

### Submitting company credit card expenses

On the 30th of each month, Expensify will auto-generate your expense report and send an email notification when the report is ready, leaving 4 main steps to prepare your report for submission: 

#### 1. Coding expenses
- In some cases, Expensify reports will show payment processor names (i.e. Stripe, PayPal) rather than the actual payee merchant, making it difficult to identify charges. This can be resolved by downloading your Amex statement from the portal, which contains greater detail.


#### 2. Required supplementary information
Certain purchases require additional data in order for the Finance team to accurately process transactions. 

Expense reports are considered incomplete if missing any of the following data:
- **Expense Tags:** If applicable, add expense tags. Common tag examples include company summits, marketing campaigns, and professional service engagements.
- **Laptops/Equipment:** Purchases of laptops and other assets in excess of $1,000 USD (per item) must include the name of the GitLabber for whom the asset was purchased. 
- **Airfare/Travel:** Purchases of flights, transportation, and lodging on behalf of other GitLabbers must also include the name of the corresponding individual.

#### 3. Attaching receipts
- Any expense over $25 USD requires a legible receipt. No exceptions.

#### 4. Submit the report
- Reports are due on the 4th business day of each month. Failure to meet these policy guidelines on an ongoing basis will result in permanent cancellation of your card.  

 **Note that company credit card expenses should never be commingled with reimbursable expense reports. Reimbursable expenses should always be submitted separately. Please contact the Accounting Manager if you have any questions.**   


## Month-End Close Checklist

- [Month-End Close Checklist](https://docs.google.com/a/gitlab.com/spreadsheets/d/1SSUQpudxxpPgXIS97Ctuj-JRII0qhq0I3r19jmBKU7c/edit?usp=sharing)

## Zuora Subscription Data Management

### Basic Assumptions

* Subscriptions should only be cancelled with 45 days of the start. Exceptions can be made (see [Support Workflows](/support/workflows/))
* Subscriptions can be linked across multiple Zuora and SalesForce (SFDC) Accounts, but not SFDC Ultimate Parent Accounts.
* All Zuora Accounts must be linked to a valid SFDC Account.
* MRR can change historically due to customer behavior (renewals, cancellations, etc.)

### New Accounts vs New Subscriptions

There are instances where a new account in Zuora is required rather than just a new subscription in an existing account. This is determined by the sold-to contact person. 

Within the customer account portal, customers can only see a single Zuora account at a time. If a customer wants to add a subscription and the contact information is the same, then the subscription can be added to the existing account. 

If a customer wants an additional subscription for a different sold to contact, then a new Zuora account will be created so that every sold to contact can log into the portal and manage their subscriptions.

### Linking Renewal Subscriptions

When a customer renews their subscription, a new subscription is typically created. This can create challenges for calculating metrics like dollar retention for a subscription because once subscription has ended and another has started. To address this, a linkage is made between the original subscription and its renewal(s). 

The field `Renewal subscription` is used to create the mapping. These are the following constraints on this field:
* This field defines a unidirectional relationship that points to a separate subscription name. 
* A renewal subscription can start on the same or future day as the original subscription start date to which it is linked to, but never in the past. 
* Any number of subscriptions can point to the same renewal subscription as long as the time constraint is met. 
* A subscription may have any number of renewal subscriptions that it points to as long as the time constraint is met. This is a one-to-many relationship. Each renewal subscription to which the original subscription is linked is input in the field and are separated by two pipes. 
  * For example, subscription [A-S00009093](https://www.zuora.com/apps/Subscription.do?method=view&id=2c92a0ff635c92e601635fdb126b3967) is linked to `A-S00009096 || A-S00009095`
* Renewal subscriptions can point to subscriptions under separate Zuora Accounts
* Renewal subscriptions can start 12 months or less after the original subscription. Practically, this is because a linkage of greater than 12 months has no effect on any relevant metrics (Retention or Yearly counts).

The process to make the linkage is as follows:
1. Cancel the old subscription in Zuora.
1. Copy and paste the new subscription in "Renewal Subscription" field with no trailing spaces.

## Sales Compensation Plan

### Plan Purpose

GitLab has introduced the GitLab Sales Compensation Plan, as specified in this Master Plan Document (the "Plan"), to develop the highest quality global organization by attracting, retaining and rewarding eligible GitLab contributors (“Participants”) who reach and exceed their sales, revenue and individual performance goals.  “GitLab” is defined as the employing/contracting legal entity of the applicable Participant (from among the GitLab group of Companies, which is comprised of GitLab Inc., GitLab BV and any of its’ direct or indirect subsidiaries or affiliates).
Except as required by applicable law, nothing in this Plan or any Participant Schedule is intended to create or amend any existing employment or contractor agreement for a particular term with any Participant.  
Participants are reminded that in addition to their eligibility for Commissions and Bonuses, they are paid a Base Salary or Base Contractor Fee, which entitles GitLab to expect that: each Participant will act in GitLab’s best interest even if it does not result in commissions or bonuses paid to them; Participants will assist others in earning business for GitLab, even if it does not result in commissions or bonuses paid to them; and that Participants will perform all other duties and responsibilities required or assigned to them.

### Definitions

Capitalized terms found in the text of the Plan shall have the following meaning:

1. “Advances Against Compensation” – Any unearned compensation (compensation that is advanced to the Participant prior to its being earned by the Participant) is considered a recoverable advance against compensation.  Examples of unearned compensation include unearned commissions resulting from credit memos, returns or unpaid balances, unearned bonuses resulting from the same, unearned wages and recoverable draws.  Advances against compensation may result in the employee incurring a negative compensation balance.  Neither commission nor bonus payments will be made to employees who have incurred a negative balance until the entire negative balance has been offset in full by earned commissions and bonuses.  The Company reserves the right to recover all advances against compensation that are not offset against commissions, bonuses or any other amounts otherwise payable to Participant.
1. "Commission" or "Commissions" means a percentage of Realized Revenue payable to the Participant (in accordance with the terms of this Plan and the applicable Participant Schedule) for services performed by GitLab and paid for by the Customer during the term of the Participant's employment with GitLab.
1. “Earned Commissions” become payable at 50% of invoiced amount at time of invoice and 50% at time of collection by GitLab during the employment of the Participant with GitLab.
1. "Base Salary" is base annual pay that is paid periodicially according to our [Global Compensation](https://about.gitlab.com/handbook/people-operations/global-compensation/) standards.
1. "OTI" is On Target Incentive compensation paid in addition to Base Salary.  This is often comprised of Commission and/or a bonus tied to achievement relative to a company assigned goal.
1. "OTE" is On Target Earnings or simply total compensation.  It is the sum: Base Salary + OTI = OTE.
2. "Base Commission Rate" refers to the commission rate paid to fully-ramped reps, or ramping reps who have exceeded their prorated annual quota.
3. "Super Commission Rate" refers to the commission rate paid to ramping reps who have not exceeded their prorated annual quota.
1. An “Existing Sales Agreement” is an agreement signed by GitLab and an existing customer of GitLab.
1. A “New Sales Agreement”  is a sales agreement signed by GitLab and a new customer to GitLab.
1. "Non-Commissionable Revenue" is any revenue received under a Sales Agreement which is not, in GitLab’s opinion, attributable to the actual products sold or services rendered by GitLab, or third party or extraordinary costs and expenses applied against such revenue.  Non-Commissionable Revenue includes, but is not limited to:
reimbursable travel or other out-of-pocket expenses;
any third-party costs or fees that GitLab passes through to the Customer, with or without mark-up;
sales taxes, duties, tariffs, and similar assessments arising in connection with the services;
extraordinary expenses incurred in connection with the product sale or performance of the services as determined by GitLab;
any non-cash consideration paid by the Customer for the services including, but not limited to, stock or stock warrants; and
Any and all costs of collection.
1. A “Participant Schedule” sets forth the details of the Participant’s commission and/or bonus payments under the Plan.  The Participant Schedule will contain the applicable commission rates, bonus rates, sales/revenue targets, methods of calculation, and/or other terms and conditions regarding the payment of commissions and/or bonuses by GitLab under the Plan.
1. “Realized Revenue” is the gross amount actually (i) invoiced to a Customer pursuant to a Sales Agreement, and (ii) is expected to become recognizable as revenue by GitLab in accordance with Generally Accepted Accounting Principles in the United States of America (“GAAP”), minus all Non-Commissionable Revenue as determined by the CFO or his/her designee. Realized Revenue includes bookings from [IACV](/handbook/finance/operating-metrics/#bookings-incremental-annual-contract-value-iacv), Renewals and Bookings from other types of revenue as may be defined on a Participant Schedule.
1. A “Sales Target” is the IACV, Total Contract Value (TCV) or Annual Contract Value (ACV) or other value established by the Company, as defined in the Participant schedule, of all sales which the Participant is expected to close (in the form of a Sales Agreement) during the calendar year, or for which the Participant is ultimately held responsible for closing during the calendar year (as may be further defined in the Participant Schedule).
1. A "Sales Agreement" is a contract (which incorporates the terms of a fully executed master agreement) signed by authorized representatives of both the Customer and GitLab, and which provides the terms and conditions pursuant to which GitLab shall deliver product or provide specific services to the Customer, as well as the specific terms of payment to be made by the Customer to GitLab for the services provided.

### Effective Date

This Plan is effective for all Commissions or Bonuses calculated for payment or otherwise earned hereunder during the Plan term as defined in a Participant schedule.  This Plan will remain in effect as the governing plan until explicitly amended or replaced by another plan.   However, each Participant Schedule shall be effective only for the period designated on each such Participant Schedule unless extended by writing (including email) by the Company.
As of the Effective Date, this Plan supersedes and replaces all prior plans regarding the payment of commissions, bonuses and similar incentive compensation by GitLab.  

### Eligibility

No GitLab contributor shall become a Participant in this Plan unless and until (i) he or she accepts the terms and conditions of this Plan and his or her individual Participant Schedule as provided by GitLab by signing such Participant Schedule and returning it to GitLab, and (ii) an authorized representative of GitLab signs and returns a fully executed Participant Schedule to the Participant.

### Requirements

The following activities may be required of the Participants in order to close a Sales Agreement and a Sales Agreement with the Customer, thus enabling the various Participants to be eligible for the payment of Commissions under this Plan.

A.	Each Participant shall, in coordination with his/her sales manager, engage in the following activities:

Provide detailed written (and input into SFDC system) information to GitLab about the Customer or prospective New Customer, as well as said Customer’s contact person(s), including: (a) the name, title, address, and phone number of such Customer and its contact person(s); (b) the approximate dates and substance of any prior discussions with such Customer relating to GitLab’s products and services; and (c) provide outlines or other documentation of such Customer's business issues, key concerns, needs, the suggested sales strategy for GitLab, and other relevant information.
Deliver presentation(s) related to GitLab’s products and services;
Develop the proof of concept or solution demo, as required, of GitLab’s product  and secure the technical recommendation from the Customer or the prospective New Customer;
Work with the appropriate GitLab personnel to establish a competitive quote for the products and services or increase the size (i.e., the price) of the Sales Agreement;
Pursue signature for the Sales Agreement (which must be pre-approved by GitLab) or accelerate the Customer’s or the prospective New Customer's signature thereof;
Assist in the collection of all invoices and expenses, and provide meaningful assistance to GitLab in the resolution of disputes or project issues with the Customer; and
Engage in any other activity requested by GitLab with the intent of promoting GitLab and its products and  services.

B.	In addition to the above, Participants that fulfill GitLab management roles shall, engage in the following activities:

Assist other GitLab personnel in any or all of the activities stated above;
Manage and supervise the proper delivery of GitLab’s products and services in accordance with the applicable terms of the Customer contract and Sales Agreement;
Maintain the morale of assigned team members and inspire excellence in the performance and delivery of GitLab’sproducts and services;
Manage and maintain relationships with Customer personnel and lay the groundwork for the sale of additional GitLab products and services to the Customer;
Maintain a constant awareness of the Customer’s technology needs and promptly advise the relevant GitLab personnel of new opportunities for the sale of additional products and services to the Customer; and
Engage in any other activity requested by GitLab with the intent of promoting GitLab and its products.
Participants in this Plan may not participate in any other GitLab commission or bonus plan unless approved in writing by the CEO or CFO.  Payments, if any, under this plan shall be made in addition to Participant’s regular compensation.
A Commission shall be deemed to be "Earned" under this Plan when, during the term of the Participant’s employment, GitLab has been paid cash by the Customer for products delivered and services rendered pursuant to a Sales Agreement for which the Participant is eligible to receive Commissions.  No Commissions or Bonuses may be Earned by a former Participant following termination of the Participant's employment with GitLab unless specified in a Participant’s Schedule or unless authorized by GitLab in writing by the CEO or CFO.

### Responsibilities and Standards of Conduct

Participants in the Plan shall perform in an ethical manner at all times.  Managers are responsible for ensuring that their employees, including new employees, are aware of these standards and their responsibility for compliance.  Any actions that violate the values of the Company, including those listed below and in the Company's policies, subject an employee to possible corrective or disciplinary action, up to and including termination.
Participants should leverage themselves in accounts through cooperative and collaborative relationships with other GitLab resources and external channel Partners. To that end, the Participant is required to:
Submit all quotes for approval that meet the conditions as highlighted in our approval processes. This includes discounts, contract language, and any non-standard terms proposed to a client/prospect.
Comply with Customer and Company nondisclosure agreements.
Utilize his or her best efforts to develop long term mutually beneficial relationships with partners and customers that reflect the Company's values.
Be respectful of competitors and not disparage their company or products.  Disclosure of competitor, third party, and business partner strategies must be fair and factual, based only on public information.
Apply the requisite diligence, effort, and knowledge in representing the Company.
Strive to make the greatest contribution possible to the sales effort and to act in the Company's best interests.
When appropriate, invite and encourage other GitLab resources into assigned accounts to maximize an opportunity.
When possible, anticipate and inform management of possible commission split situations.  All commission splits must be approved by management in writing.
Participants should not compete against other GitLab’s Participants for sales opportunities for account control or in an effort to increase commissions at the expense of another Participant.  Any potential or perceived conflicts between Participants shall be immediately referred to the Participants’ supervisor(s) for prompt and fair resolution.

### Plan Description

Participants are assigned to specific areas of responsibility, which may be changed at any time in GitLab’s sole discretion.  Areas of responsibility may be established based on industry specific customers or prospects, selected market segments, specific geographical areas, or any combination of the above criteria or any additional criteria established in GitLab’s sole discretion.  Sales Targets (as defined below) for each Participant are designed to support established business plan objectives and/or are a function of Participant’s prior target and goal attainment, expected future target and goal attainment, and the company’s growth and strategic requirements.

### Commissions

1.	Participants are eligible to receive Commissions (solely to the extent specified in each Participant Schedule as defined below) based in general on (a) successful performance of the duties set forth above; and/or (b) sales and/or revenue from New Customers; and/or (c) sales and/or revenue from Existing Customers.
1.	A Participant may also be eligible to receive a split Commission as detailed later in this Plan.
1.	Goal Setting:
(a)	Each Participant's Sales Targets, if any, will be established and set forth in the Participant Schedule.  Sales Targets (as well as any other targets established from time-to-time) will be established for each Participant by the Participant’s manager in conjunction with GitLab’s Chief Financial Officer (“CFO”).
1.	New and Existing Customers as a Basis for Commission Calculations:
(a)	"Customer" is defined as a "company" or "firm" for whom GitLab provides goods or services pursuant to a Sales Agreement.  Customer is not defined as a Customer contact person, and the term Customer does not include any parent, subsidiary, division or affiliate of that Customer, even if controlled by Customer or under common control with Customer, unless otherwise determined by GitLab in its sole discretion.
(b)	A "New Customer" is defined as any Customer or division within a Customer with which GitLab has not done any business (e.g., delivered goods or services to or received revenue from such Customer) during the 12-month period immediately preceding the first product sale or commencement of services as specified in a Sales Agreement.  A Customer remains a "New Customer" for the 12-month period immediately following product delivery or the commencement of services as specified in the first New Customer Sales Agreement.  Realized Revenue resulting from any other Sales Agreement signed with said New Customer during the 12-month period immediately following the commencement of services under the first New Customer Sales Agreement will also qualify as a New Customer Realized Revenue under the Participant Schedule (if applicable).
(c)	A Customer is an "Existing Customer" if it has done business with GitLab at any time during the 12-month period immediately preceding the product delivery or commencement of services as specified in a Sales Agreement (and it is not a New Customer).   A New Customer becomes an Existing Customer at the end of the 12-month period immediately following the commencement of services as specified in the first New Customer Sales Agreement.  The 12-month period specified above shall be subject to change in the event that the Plan is modified, superseded, or terminated by GitLab.
1.	Timing and Calculation of Commission Payment:
(a)	Although some Commission rates may be based on the achievement of various Sales Targets, the Participant's eligibility to receive Commission payments from GitLab will be based on Realized Revenue actually paid to GitLab by the Customer, during the term of Participant's employment, pursuant to a Sales Agreement (less Commissions paid on returned Realized Revenue). Therefore, Participants are strongly encouraged to structure their activities and act in a manner that is conducive to prompt payment by the Customer. The above notwithstanding, GitLab, in its sole discretion may make Advances Against Compensation as described herein.
(b)	Commissions Earned on Realized Revenue paid by the Customer shall be calculated by reference to the following formula:

Commissions = A – B – C
For the purposes of this formula:
“A” means the amount payable in respect of Realized Revenue, calculated in accordance with the terms and Commission Rates set forth in the applicable Participant Schedule;
“B” means the amount of any Commissions paid on returned Realized Revenue (as determined by GitLab) or third party commissions such as third party software vendors or resellers; and
“C” means the amount of any Advances Against Compensation.
(c) Commissions Earned on Realized Revenue paid by the Customer, and advanced Commission payments based on revenue recognized and invoiced pursuant to Section 5(a) above, will be calculated on a monthly basis.
(d)	Eligible Commissions will be paid within 30 days of the end of the applicable month. Specific payment date(s) will be stated on individual's participant schedule.  

### Importance of Cash Collection

Our policy is to pay commissions 100% upon invoice issuance. This policy aligns the interests of the Company and the Participant to collect cash as quickly as possible. Industry average cash collection cycles are 75 days. At GitLab our history and continued goal is to collect cash in less than 45 days. By keeping our accounts receivable balance to a minimum we can raise less money which means less dilution (note that the amount of capital saved is equal to one month's [TCV]("/handbook/finance/operating-metrics/#bookings-total-contract-value-tcv") as reported in our monthly metrics dashboard). By being capital efficient we create increased value to our stockholders and team member option holders. The sales team plays a critical role in the collection process. Through our experience we have found that 90% of invoicing delays are due to factors controllable by the sales team members, including:

1. Providing the correct email address and contact information for the customer billing contact.
1. Determining whether a PO is required and, if required, having secured the PO at time of invoice.
1. Encouraging use of payment due upon receipt collection terms or not waiving our standard 30 day collection terms.

Please refer to our sales handbook on how to [process orders]("/handbook/sales/#processing-orders") to expedite the collection process.

### Non Commission Incentives

Participants may also from time to time be eligible to receive a discretionary bonus under the Plan (individually a "Bonus", or referred to collectively as "Bonuses").  Such Bonuses will be identified on the Participant Schedule. The inclusion of a target Bonus percentage on a Participant Schedule is not a guarantee that a Bonus will be paid, or that any Bonus that is paid will meet the target percentage. Although discretionary Bonus payments (if any) are calculated, in part, based on past performance criteria (as described below), Bonus payments are provided by GitLab solely to motivate the Participant during the current calendar year.  Bonus payments shall be made in the sole discretion of GitLab, and Bonuses are earned only when paid by GitLab.  Bonuses will be paid (if at all) as soon as reasonably practicable following the close of the  bonus.

### General Administrative Provisions

1.  This Plan supersedes all prior commission, bonus or incentive compensation plans applicable to Plan Participants.  The provisions of any prior commission, bonus or incentive compensation plan not specifically addressed in this Plan shall no longer operate, nor shall said provisions be considered part of the Plan.  Please also refer to the section entitled “Effective Date” above.
1.  GitLab reserves the right to interpret, reinterpret, amend, modify, extend, or terminate the Plan and any Participant Schedule, in whole or in part, at any time.  This right includes, but is not limited to, any and all decisions regarding salaries, Commissions, Bonus eligibility, Sales Targets, revenue attainment, sales assignments or areas of responsibility such as assigned industries, Customers, prospective Customers, market segments, specific geographical areas, or any other Commission or Bonus-related classifications and determinations, or any other matter affecting the Participant's employment.  GitLab reserves the right to adjust Sales Targets for any or all Participant's during the year if, in GitLab’ sole opinion, significant changes occur with regard to the industry, GitLab’ assessment of the industry, or GitLab’ position within the industry.  GitLab further reserves the right to adjust Sales Target and Realized Revenue determinations, including decisions regarding the attainment of such targets or levels by any Participant and the corresponding right to adjust or offset Commissions paid or payable and the applicable Commission rates based on such determinations.  GitLab will make decisions related to position transfers that may affect Participants on a case-by-case basis.  All such interpretations, amendments, modifications, extensions, adjustments, changes and/or revisions to the Plan or a Participant Schedule shall be binding on the Participant.  Notification of any such changes to the Plan or any Participant Schedule may be made in writing or by electronic mail to the affected Participants.  In all such matters of administration of the Plan, the CEO, CFO (or the CFO’s designee) shall have final responsibility and authority.  All decisions of the CEO or CFO in interpreting this Plan or a Participant Schedule shall be final and binding.  
1.  If (i) any Realized Revenue is subsequently returned (whether as a cash refund or credit for future services) to the Customer for any reason, (ii) any revenue recognized and invoiced, and used by GitLab to calculate and pay an advance Commission payment and/or Advances Against Compensation pursuant to Section 5(a) above, remains unpaid by the Customer after 90 days following the due date of the applicable invoice, or (iii) GitLab’ recognition of Realized Revenue is reversed, then all Commissions paid or payable with respect to such revenues shall be reversed and deducted from the calculation of future Commissions in accordance with the formula in Commissions, Section 5(b) above, or otherwise offset against other Commissions due and payable.  Credit memoranda and write-offs also represent returned revenue that may result in a Commission reversal to be determined at the sole discretion of the CFO.  No Commissions shall be paid on non-purchased credits consumed and invoiced.  The reversal of any unpaid Commissions shall result in a reduction of the Commissions due to the Participant.  The reversal of any paid Commissions shall be charged to the Participant’s account and set off against future Commission payments unless otherwise approved by the CFO.  Any reversal of Commissions and the corresponding Realized Revenue may also result in a reduction and adjustment of the Participant's attainment levels relative to Realized Revenue and Sales Targets attainment, and the applicable Commission percentages paid or payable on a Sales Agreement or Realized Revenue.
1.  If a Participant disagrees with the calculation of Commission payments or any other decision by GitLab hereunder, the Participant may appeal the decision by submitting an explanation, in writing or by e-mail, to the Participant’s assigned manager and the CFO.  All such appeals must be submitted within 90 days of the original GitLab decision forming the basis of the appeal or the Commission payment or non-payment.  Within a reasonable period of time, not to exceed forty-five (45) days following the submission of the appeal, the CFO will issue a final decision regarding the appeal.  All such decisions shall be final and binding on GitLab and the Participant.  
1.  Participants are not authorized to accept or enter into contracts, purchase orders or Statements of Work on behalf of GitLab or otherwise legally bind GitLab in any way.  All contracts and Statements of Work must be presented to the GitLab Legal Department and the CFO (or the CFO’s designee) for review, processing and authorization.  Only the CFO and the CEO of GitLab are authorized to sign and accept contracts on behalf of GitLab, unless specific written authorization to do so is provided by one of the foregoing officers.   GitLab reserves the right to reject any proposed contract or Sales Agreement for any reason at its sole discretion.
1.  GitLab’ general policy is to allocate Commissions for each Sales Agreement only to the Participant most responsible for closing the Sales Agreement (as determined in GitLab’ sole discretion). Commissions eligibility may also be based upon the closing of business by the Participant's assigned team (as may be further defined in the Participant Schedule).  If a team revenue target applies (as specified in the Participant Schedules of the team members), or in certain other highly exceptional circumstances in which multiple Participants worked cooperatively with each other and such Participants provided significant assistance in all phases of the sales effort, a Commission may be split among the cooperating Participants (which may also include in certain circumstances, as determined in GitLab’s sole discretion, the splitting of Commissions for sales made, and revenue received by, GitLab’s affiliated companies).  The total split Commission will not be greater than the total Commission that could be earned by any Participant individually.  In any such situation, including the transitioning of Customer accounts from one Participant to another, the CFO will determine the appropriate allocation of Commissions among the Participants.  A Participant’s referral of a lead to another Participant or general networking activities (including activities at trade shows) will not warrant the sharing of Commissions.
1.  If a Participant's employment with GitLab is terminated by the Participant or GitLab, regardless of the reason or cause for such termination (if any), then, except as stated in this Section, the Participant will be paid only for those Commissions Earned through the Participant's last day of employment, provided that, all other terms and conditions of the Plan have been satisfied.  Any Earned Commissions which have not been paid to the former Participant as of his or her termination date may be withheld, at GitLab’s discretion, for up to ninety (90) days following termination of the Participant in order to accommodate potential Commission reversals and other adjustments.  GitLab shall be entitled to recover any unearned advance and/or Advances Against Compensation made intentionally or inadvertently against Commissions, unless the advance is specifically approved as a salary supplement.  If, within 90 days of the invoice date, the Customer does not pay for the services relative to any Commissions advanced by GitLab, at GitLab’s request the Participant agrees to reimburse GitLab for the Commissions advanced.  Advances Against Compensation balances on record upon a Participant’s termination of employment, for any reason, will be collected from the employee's Base Salary or from any other compensation payable to the employee.  Participants electing to receive Advances Against Compensation agree (a) that Participant authorizes GitLab to offset such deficit balance against any unpaid salary, wages (and accrued but unpaid vacation) or travel expense reimbursement owing to Participant upon termination of Participant’s employment with GitLab (b) that upon termination of their employment any Deficit Balance may be deducted from Participant’s final paycheck and (c) the employee will reimburse GitLab for any remaining balance on record with personal funds.
1   Since Bonuses are solely discretionary and provided to motivate the Participant during the current calendar year, if the Participant's employment with GitLab is terminated by the Participant or GitLab, regardless of the reason or cause for such termination (if any), or if the Participant gives notice of termination of the Participant’s employment, prior to the date that the Bonus is paid, the Participant will not receive any Bonus payments whatsoever.
1.  Taxes will be withheld from all Commission and Bonus payments in accordance with applicable laws.
1.  If any of the provisions of this Plan or a Participant Schedule shall be determined to be invalid or unenforceable, such invalidity or unenforceability shall not render the entire Plan invalid or unenforceable.  In such event, the provisions of the Plan or Participant Schedule so affected shall be modified and/or restricted only to the extent necessary to bring them within legal requirements, and the remainder of the Plan, the Participant Schedule, and the affected/modified provisions shall be construed and enforced accordingly.
1.  The terms and conditions of this Plan, together with the applicable Participant Schedule, constitute the complete and exclusive statement of the understanding between GitLab and each Participant, which supersedes and excludes all prior or contemporaneous proposals, understandings, agreements or representations, oral or written, between GitLab and each Participant with respect to the subject matter of bonuses and commissions.
1.  For all Participants employed by GitLab in the United States, the terms and conditions this Plan, together with the applicable Participant Schedule, shall be governed by California law, excluding its choice of law principles and the choice of law principles of any other jurisdiction.  Any action or proceeding arising out of or related to this Plan or the Participant Schedule shall be brought only in the Circuit Court of San Francisco, California or the United States District Court, San Francisco. By participating in this Plan, GitLab and each Participant hereby consent to such venue and to the jurisdiction of such courts over such proceeding and themselves.
1.  For all Participants employed by GitLab in any country other than the United States, the terms and conditions of this Plan, together with the applicable Participant Schedule, shall be governed by the laws of the country in which the Participant is employed or legal entity with which the Participated is contracted, excluding its choice of law principles and the choice of law principles of any other jurisdiction.  Relevant documents in other languages shall be translated into English.  Examination of witnesses by the parties and the arbitrators shall be permitted.  The award of the arbitrators shall be final and irrevocable and the arbitrators shall state the reasons upon which the award is based.  The parties agree that judgment upon the award rendered in such arbitration may be entered in any proper court of competent jurisdiction. The costs of arbitration and those related thereto, including reasonable legal fees, shall be borne by either or both of the parties in whatever proportion as the arbitrators may award
1.  GitLab reserves the right to limit Commissions paid to the Participant on a deal-by-deal basis for windfall situations that would result in extraordinarily high payouts to the Participant without a commensurate business development effort on the part of the Participant.  In all such instances, GitLab will strive to be fair to the Participant while protecting GitLab from unjustifiable Commission and Bonus expense.  All such decisions by GitLab shall be final and binding on Participants.
1.  If a Participant Schedule modifies or contradicts the terms and conditions stated herein and the Participant Schedule does not specifically reference and acknowledge the contradiction, it is the Participant's responsibility to immediately notify GitLab upon discovery of such discrepancy, and GitLab shall, in its sole discretion, modify the terms of said Participant Schedule or the terms of this Plan as GitLab deems reasonable in light of such discrepancy.
1.  For all Participants employed by GitLab in any country other than the United States, the terms and conditions of this Plan may be modified or supplemented by a Country Addendum to this Plan.  Please see the applicable Country Addendum, if any, for the country in which the Participant’s employer is based.<br>

## Accounting Policies

## Assets

### Capital Assets Policy

#### Purpose
This policy establishes the minimum cost (capitalization amount) used to determine the capital assets recorded in GitLab's financial statements.

#### Capital Assets Defined

A “Capital Asset” is a unit of property that has an economic useful life extending beyond 12 months **and** was acquired (or in some cases, produced) for a cost of [$1,000 (USD)](/handbook/people-operations/global-compensation/#exchange-rates) or more. Capital Assets must be capitalized and depreciated for financial reporting purposes.

#### Capitalization Thresholds

GitLab establishes [$1,000 (USD)](/handbook/people-operations/global-compensation/#exchange-rates) as the minimum amount required for capitalization. Any item with a cost below this amount or an economic useful life of 12 months or less, is expensed on the date of purchase.

#### Methodology 

All capital assets are recorded at historical cost as of the acquisition date. These assets are depreciated on a straight-line basis, with the number of depreciation periods being determined by asset class.

* **Equipment:** For our purposes, equipment generally consists of computers and other related office tools. Equipment under our INC entity is assigned a standard useful life of three (3) years. However, equipment under our BV entity is depreciated over five (5) years due to Dutch tax laws, which limit depreciation of capitals assets to a maximum of 20% of the asset cost per year. The following link contains additional information on Dutch tax laws surrounding capital and fixed  assets: [Netherlands Capital and Fixed Assets Guide](http://www.ey.com/gl/en/services/tax/worldwide-capital-and-fixed-assets-guide---xmlqs?preview&XmlUrl=/ec1mages/taxguides/WCFAG-2017/WCFAG-NL.xml)

* **Furniture:** Furniture includes office furniture and other fixtures. The standard useful life for furniture is seven (7) years. This depreciation schedule applies to all entities.

Invoices and purchase receipts for capital assets are retained for a minimum of five years.

### Investment Policy

 #### Purpose
The purpose of this policy is to establish the responsibility, authority and guidelines for the investment of operating surplus cash.  Surplus cash is defined as those funds exceeding the operating requirements of the Company and not immediately required for working capital or near term financial obligations.

#### Scope
This policy shall apply to the Company and all subsidiaries.  This investment policy will be reviewed periodically to ensure that it remains consistent with the overall objectives of the Company and with current financial trends.

#### Approved Brokerage Institutions
The Company may use the following brokerage institutions:

1. Morgan Stanley Smith Barney LLC
1. Comerica Securities, Inc.

#### Investment Objectives 

The basic objectives of the Company’s investment program are, in order of priority:
Safety and preservation of principal by investing in a high quality, diversified portfolio of securities, mutual funds, and bank deposits.
Liquidity of investments that is sufficient to meet the Company’s projected cash flow requirements and strategic needs.
Maximize after-tax market rates of return on invested funds that are consistent with the stated objectives herein, conservative risk tolerance and the Company’s current tax position.
Maturity Limits
Individual security maturities should not exceed 24 months.  The weighted average maturity of the portfolio shall not exceed 12 months.  A maturity or effective maturity by definition shall include puts, announced calls or other structural features which will allow the Company to redeem the investments at a quantifiable price consistent with liquidity, safety and preservation of capital.
 
#### Eligible Investments
United States Government Securities:
Marketable debt securities which are direct obligations of the U.S.A., issued by or guaranteed as to principal and interest by the U.S. Government and supported by the full faith and credit of the United States.
United States Government Agency Securities:
Debt securities issued by the Government Sponsored Enterprises, Federal Agencies and certain international institutions which are not direct obligations of the United States, but involve Government sponsorship and are fully guaranteed by government agencies or enterprises, including but not limited to:
·   	Federal Farm Credit Bank (FFCB)
·   	Federal Home Loan Bank (FHLB)
·   	Federal Home Loan Mortgage Corporation (FHLMC)
·   	Federal National Mortgage Association (FNMA)
 
 
### Money Market funds
 
Money Market Funds must be rated AAA or equivalent by at least one NRSROs.
 
At time of purchase investment restrictions:

Investment Products (Rating, Sector Concentration, Issuer Concentration)
 
1. US Government (AA+, No Sector Concentration, No Issue Concentration)
1. Agency (AA+, 50% Sector Concentration, 10% Issuer Concentration)
1. Money Market Funds - US Government/Treasury (AAA, No Sector Concentration, 50% Issuer Concentration)

### Prepaid Expense Policy

#### Purpose
This policy describes the methodology used to monitor and account for GitLab’s prepaid expenses.

#### Prepaid Expenses Defined
A [*Prepaid Expense*](https://www.investopedia.com/terms/p/prepaidexpense.asp?ad=dirN&qo=investopediaSiteSearch&qsrc=0&o=40186) arises when a cash disbursement is made for goods and services prior to realizing the associated benefits of the underlying goods and services. These transactions are recorded as assets until the goods and services are realized, at which point an expense is recorded. Our minimum threshold for recording prepaid expenses is [$1,000 USD](/handbook/people-operations/global-compensation/#exchange-rates). Anything under this amount is expensed immediately.

#### Identification and Recording of Prepaid Expenses 
Once a purchase request makes it through the [company approval workflow](/handbook/finance/procure-to-pay/), Finance will take the following steps to ensure prepaid expenses are recorded accurately: 

1. The Accounts Payable Administrator flags all bills that qualify as prepaid expenses during the normal course of processing bills in the AP mailbox.

1. The flagged bills are then analyzed and added to the asset register located in Google drive. Information includes expense category, department, benefit period, and amount to be amortized. 

1. At the end of each month, the Accountant reconciles the prepaid additions to the general ledger, which includes verifying amortization schedules and amounts.  

Amortization is recorded straight line based on a mid-month amortization method as follows:
If the first month of service begins on the 1st to the 15th of the month, a full month amortization will be recorded in the current month.  If the first month of service begins on the 16th to the last day of the month, amortization will begin on the 1st day of the subsequent month.

Mid-Month Amortization Method does not apply to prepaid expenses with a monthly amortization equal to or greater than 50,000 USD or if the amortization if spread only over 1 period.  If monthly amortization is equal to or more than 50,000 USD, the first month amortization will be calculated based on actual number of days where services were rendered.

Prepaid Not Paid:  For any prepaid expenses not processed for payment, an adjustment for "prepaid not paid" is posted to the respective prepaid expense account and AP manual adjusment account (GL Account 2001).  A prepaid expense is not treated as an asset if a liability remains in the AP sub-ledger.  Prepaid not paid adjusments are performed on a quartly basis at minimum.
Any deposits paid which will be held for more than 12 months such as security deposits or deposits to retain consultants will be booked to Security & Other Deposits (GL Account 1620)

Prepaid Bonuses with a Clawback will be recoreded to Prepaid Bonus w/Clawback (GL Account 1152) and will be amortized in accordance with the bonus agreement terms, using the mid-month convention.

4. Finally, the balance is reviewed one last time when the Controller performs a review of the financials prior to closing the period.

#### Contribute related expenses
Team member travel expenses are expensed in the period incurred. Costs related to third party vendors such as hotels, facilities, excursions are recorded to prepaid expenses and recognized as expense at the time of the event.

## Liabilities

### Accrued Liabilities Policy

#### Purpose
To provide clear guidance concerning the identification and recording of items included in GitLab’s accrued and other liability accounts. The purpose of monthly accrual processes is to allocate expenses to the proper accounting period and match expenses with related revenues. At the close of each month, accrual processes ensure that all expenses related to that month are correctly included in the company's financial statements. Additionally, this policy establishes standards and guidelines for ensuring that GitLab accounts for monthly accruals in a manner that is compliant with management's objectives and generally accepted accounting principles (GAAP). This policy applies to GitLab and all subsidiaries.

#### Identification
We require that all expenses be recorded, to the greatest degree practical, in the period they are incurred. The accrual process should be completed on a monthly basis to ensure liabilities are recorded accurately in their respective periods.

The following items should be accrued monthly as necessary (note: this list is not all-inclusive):
- Accounts Payable:
	* Contracts: Amounts due under contracts, including retainer fees. These items should be recorded as they become billable.
	* Professional Fees: This liability includes audit, legal, consulting and other professional fees.
	* Legal Contingencies: Pending or threatened litigation, and actual or probable settlement. Legal contingencies should be determined with the help of GitLab’s Senior Director of Legal Affairs.
- Wages and Compensation:
	* Team Wages: This includes employee wages and independent contractor fees.
	* Commissions: Liabilities arising from commission obligations to team members who are eligible for commission compensation.
	* Bonuses: Liabilities related to bonus payments for GitLab team members.
	* Taxes: All employment taxes required for statutory compliance that relate to the GitLab team.
- Any other material obligation not mentioned above that is a liability of GitLab

#### Timing

Obligations that accrue over time are recorded throughout the accounting period in a methodical and rational manner. Obligations that accrue when an event occurs should be recorded at the time of the event.

Factors that are considered in determining the time of recording accrued liabilities include:

1. Risks of ownership passed to GitLab through receipt of goods or services. 
2. The expense must have been incurred during the month being closed; that is, the product or service must have been received on or before the last day of the month in order to qualify as an expense. 
3. Even though an expense may have been initially budgeted in the month, it is not eligible for accrual unless the company received the product or service.
4. Accruals are reversed in the next month and re-accrued the following month, as needed.
5. If payment is due prior to receiving goods or services, the amount should be accrued to prepaid expenses.

#### Procedural
The Finance team is responsible for having procedures in place to reconcile accounts monthly and for keeping documentation to support accrued liabilities. Payables and accrued liabilities are recorded at face value, plus or minus any applicable adjustments. In most cases, the payable amount can be determined from the vendor bill. If not, then the amount should be verified against any relevant documents before recording the liability. When actual values are not available, the recorded value should be based on best available estimates. Estimates should be based on current market price and experience/history. 

The Controller is responsible for performing an overall review of accrued liabilities, one to three business days after accounts payable closes each month, to help ensure that all expenses are captured accurately.


#### Please see [Procure-to-Pay](/handbook/finance/procure-to-pay/)
 
## Equity

### Foreign Currency Translation Policy 

#### Overview
Foreign currency translation describes the method used in converting a foreign entity's functional currency to the reporting entity's financial statement currency. Prior to translating the foreign entity’s financial statements into the reporting entity’s currency, the foreign entity’s financials must be prepared in accordance with generally accepted accounting principles (GAAP), specifically under [Financial Accounting Standards Board (FASB) Statement No.52](http://www.fasb.org/summary/stsum52.shtml). GitLab’s financial statement reporting currency is USD. The functional currency of our non-U.S. subsidiaries is the local currency. Changes in foreign currency translation are part of accumulated other comprehensive income (loss), which is reported in the consolidated statement of equity and ultimately carried over to the consolidated balance sheet, under equity.  

#### Exchange Rates
Exchange rates used in the currency translation process vary across the three primary financial statement components:

 - **Assets and Liabilities:**  Exchange rate between functional currency and reporting currency at period-end.
 - **Income Statement:**  The exchange rate on the transaction date, if known, when revenues/expenses are earned/incurred, and recognized in the financials.
 - **Equity:** The historical exchange rate at the date when entry is made to shareholder's equity. Changes in retained earnings are based on historical exchange rates of each period's income statement.

#### Transaction Risk vs Translation Risk

##### Currency Transaction Risk 
Currency transaction risk is due to company transactions denominated in foreign currencies. These transactions must be restated into the entity functional currency equivalents before they can be recorded. Gains(losses) are recognized when a payment is made or interim balance sheet dates.

##### Currency Translation Risk 
Currency translation risk occurs due to the company owning assets and liabilities denominated in a foreign currency.

#### Cumulative Translation Adjustment 
A cumulative translation adjustment (CTA) is an entry to the comprehensive income section of a translated balance sheet that summarizes the gains(losses) resulting from exchange rate differences over time. Currency values shift constantly, affecting how a currency is valued against others. The CTA is a line item in the consolidated balance sheet that captures gains(losses) associated with international business activity and exposure to foreign markets. The corresponding gain(loss) can be found in the consolidated income statement under Other Income(Loss). CTA’s are required under GAAP since they help distinguish between actual operating gains(losses) and those that arise from the currency translation process. Additional information on our reporting standards surrounding CTA's can be found in [FASB Topic 830, "Foreign Currency Matters."](http://www.fasb.org/jsp/FASB/Document_C/DocumentPage?cid=1176162203697&acceptedDisclaimer=true)


#### Recording CTA
Exchange rate gains and losses for individual transactions are captured automatically by our ERP system, NetSuite. However, a CTA entry must be made in order to properly distinguish currency translation gains(losses) from other general gains(losses) in the consolidated financial statements. This entry includes reconciliation of any intercompany activity that generates foreign exchange gains(losses). The CTA is made on a monthly basis as part of our financial statement reporting cycle.


## Other 

### Chart of Accounts Policy

#### Scope
This policy establishes GitLab’s guidelines regarding the structure, responsibilities and requirements underlying the [chart of accounts (COA).](https://www.investopedia.com/terms/c/chart-accounts.asp)

#### Purpose
This policy establishes formal responsibilities and accountabilities for how GitLab handles requests for new, modified or closed data elements on the COA. The Controller is responsible for all aspects of financial accounting and reporting, and governs the COA.  All requests for new or modified (including closure/deactivation) COA segments, hierarchies, and configuration attributes are subject to approval by the Finance team.

#### Changes to the COA
All requests for new or modified accounts must be submitted to the Accounting Manager for review and approval through a request using the Finance issue tracker.

There are other stakeholders associated with the COA that may influence certain business decisions or financial system configurations. The Controller and Accounting Manager will include selected stakeholders in the related procedures and processes when and if appropriate. Potential stakeholders include, but may not be limited to:
- Financial Planning and Analysis
- Data & Analytics
- Other departments who have shared functionalities within the financial system

The general ledger attributes subject to this policy will be defined by the Controller based upon factors including but not limited to:
- Creating and maintaining consistency for the structure of accounts
- Standard accounting policies and practices
- Regulatory compliance requirements and reporting needs
- Financial and operational reporting needs and requirements
- External accounting and financial reporting requirements

Once an amendment to the COA has been approved, the Accounting Manager will ensure the necessary changes are implemented by updating and then closing the issue.

#### Administration
The COA is maintained in NetSuite. Changes to the COA can only be made by the Controller and/or Accounting Manager. 
